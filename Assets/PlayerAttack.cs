﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Code.Networking;
using Shared.Packets;
using Code.Utility;
using LiteNetLib;

public class PlayerAttack : MonoBehaviour
{
    private float timeBtwAttack;
    public float startTimeBtwAttack;

    public Transform attackPos;
    public float attackRange;
    public LayerMask whatIsEnemy;
    public CharacterStats myStats;

    void Awake()
    {
        myStats = GetComponent<CharacterStats>();
    }

    // Update is called once per frame
    void Update()
    {
        if (timeBtwAttack <= 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemy);

                foreach (Collider2D enemy in enemiesToDamage)
                {
                    PlayerManager playerManager = enemy.GetComponent<PlayerManager>();
                    if (playerManager != null)
                    {
                        LiteNetClient.Instance.SendPacket(new AttackPlayerPacket()
                            { PlayerId = playerManager.me.id, KnockbackX = 0, KnockbackY = 0 }, DeliveryMethod.ReliableOrdered); // TODO Calculate knockback
                    }
                }
            }
            timeBtwAttack = startTimeBtwAttack;
        }
        else
        {
            timeBtwAttack -= Time.deltaTime;
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }
}
