﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Code.Managers;
using Code.Utility;

public class HazardZone : Singleton<HazardZone>
{

    private Vector3 circleSize;
    private Vector3 circlePosition;
    private Vector3 targetCircleSize;
    private Vector3 targetCirclePosition;
    private float circleShrinkSpeed;
    [SerializeField]
    private Transform redCircle;
    [SerializeField]
    private Transform whiteCircle;
    private float shrinkTimer;

    public override void Awake()
    {
        base.Awake();
        this.circleShrinkSpeed = 100f;

        SetCircleSize(new Vector3(0,0), new Vector3(6000,6000));
        SetTargetCircle(new Vector3(0, 0), new Vector3(6000, 6000), 100000000f);

    }

    // Update is called once per frame
    void Update()
    {
        shrinkCircle();
    }

    private void SetCircleSize(Vector3 position, Vector3 size)
    {
        circlePosition = position;
        circleSize = size;
        redCircle.localScale = size;
        redCircle.position = position;
        transform.position = position;
    }

    public void SetTargetCircle(Vector3 position, Vector3 size, float shrinkTimer)
    {
        this.shrinkTimer = shrinkTimer;
        whiteCircle.localScale = size;
        whiteCircle.position = position;
        targetCircleSize = size;
        targetCirclePosition = position;
    }

    public void shrinkCircle()
    {
        shrinkTimer -= Time.deltaTime;

        if (shrinkTimer < 0)
        {
            Vector3 circleMoveDir = (targetCirclePosition - circlePosition).normalized;
            Vector3 newCirclePosition = circlePosition + circleMoveDir * Time.deltaTime * circleShrinkSpeed;
            Vector3 sizeChangeVector = (targetCircleSize - circleSize).normalized;
            Vector3 newCircleSize = circleSize + sizeChangeVector * Time.deltaTime * circleShrinkSpeed;

            SetCircleSize(newCirclePosition, newCircleSize);

            //float distanceTestAmount = 0.1f;
            //if (Vector3.Distance(newCircleSize, targetCircleSize) < distanceTestAmount && Vector3.Distance(newCirclePosition, targetCirclePosition) < distanceTestAmount)
            //{
            //    GenerateTargetCircle();
            //}
        }
    }

    //private void GenerateTargetCircle()
    //{
    //    float shrinkSizeAmount = Random.Range(20f, 40f);
    //    Vector3 generatedTargetCircleSize = circleSize - new Vector3(shrinkSizeAmount, shrinkSizeAmount);

    //    Vector3 generatedTargetCirclePosition = circlePosition + new Vector3(Random.Range(-shrinkSizeAmount, shrinkSizeAmount), Random.Range(-shrinkSizeAmount, shrinkSizeAmount));
    //    float shrinkTime = Random.Range(1f, 6f);

    //    SetTargetCircle(generatedTargetCirclePosition, generatedTargetCircleSize, shrinkTime);
    //}

    private bool isOutsideCircle(Vector3 position)
    {
        return Vector3.Distance(position, circlePosition) > circleSize.x * .35f;
    }

    public static bool isOutsideCircle_Static(Vector3 position)
    {
        return HazardZone.Instance.isOutsideCircle(position);
    }

}
