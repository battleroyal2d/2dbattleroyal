﻿using Code.Networking;
using Code.Utility;
using UnityEngine.Windows;

namespace Code.Managers
{
    public class ApplicationManager : Singleton<ApplicationManager>
    {
        public void Start()
        {
            SceneManagementManager.Instance.LoadLevel(SceneList.START_MENU, (levelName) => { });
        }

        public void switchScene(string loadScene, string unloadScene = null)
        {
            SceneManagementManager.Instance.LoadLevel(loadScene, (levelName) =>
            {
                if (unloadScene != null)
                {
                    SceneManagementManager.Instance.UnLoadLevel(unloadScene);
                }
            });
        }

        public void OnApplicationQuit()
        {
            if (Directory.Exists("Assets/Items"))
            {
                Directory.Delete("Assets/Items");
                Directory.CreateDirectory("Assets/Items");
            }
            LiteNetClient.Instance.Dispose();
        }
    }
}
