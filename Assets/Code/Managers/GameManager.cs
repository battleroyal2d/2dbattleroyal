﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Code.Managers;
using Code.Utility;
using Code.Networking;
using Shared.Models;
using Shared.Packets;
using UnityEngine.Experimental.PlayerLoop;

namespace Code.Managers
{
    class GameManager : Singleton<GameManager>
    {
        public GameObject playerPrefab;
        public Transform networkContainer;
        private Dictionary<int, NetworkIdentity> serverObjects;
        private Dictionary<int, GameObject> activePlayers;
        private Dictionary<int, PlayerManager> activePlayerManagers;

        public ConcurrentQueue<int> playersToDelete;

        public ItemPickup[] allItems;
        public ConcurrentQueue<PickUpPacket> itemsToDelete = new ConcurrentQueue<PickUpPacket>();

        public void Start()
        {
            allItems = FindObjectsOfType<ItemPickup>();

            serverObjects = new Dictionary<int, NetworkIdentity>();
            activePlayers = new Dictionary<int, GameObject>();
            activePlayerManagers = new Dictionary<int, PlayerManager>();
            playersToDelete = new ConcurrentQueue<int>();


            Player localPlayer = DataManager.Instance.Get<Player>("Player");
            GameObject myChar = GameObject.FindWithTag("LocalPlayer");
            myChar.name = localPlayer.name;
            NetworkIdentity localNetworkIdentity = myChar.GetComponent<NetworkIdentity>();
            PlayerManager localPlayerManager = myChar.GetComponent<PlayerManager>();
            localNetworkIdentity.SetControllerID(localPlayer);
            localPlayerManager.isControlling = localNetworkIdentity.isControlling;
            localPlayerManager.StartTickUpdate();

            List<Player> players = DataManager.Instance.Get<List<Player>>("OnlinePlayers");
            foreach (Player player in players)
            {
                GameObject go = Instantiate(playerPrefab, networkContainer);
                go.name = player.name;
                NetworkIdentity ni = go.GetComponent<NetworkIdentity>();
                ni.SetControllerID(player);
                go.GetComponent<PlayerStats>().enabled = false;
                serverObjects.Add(player.id, ni);
                activePlayers.Add(player.id, go);
                activePlayerManagers.Add(player.id, go.GetComponent<PlayerManager>());
            }

            StartCoroutine(CheckForLastUpdates());
        }

        public void Update()
        {
            while (!playersToDelete.IsEmpty)
            {
                playersToDelete.TryDequeue(out var playerId);
                PlayerQuit(playerId);
            }

            while (!itemsToDelete.IsEmpty)
            {
                itemsToDelete.TryDequeue(out var item);
                foreach (ItemPickup itm in allItems)
                {
                    if (item.ItemId == itm.UniqueHash)
                    {
                        Destroy(itm.gameObject);
                    }
                }
            }
        }

        public void RemoveObject(PickUpPacket toDelete)
        {
            allItems = FindObjectsOfType<ItemPickup>();

            itemsToDelete.Enqueue(toDelete);
        }

        public void HidePlayerIfNeeded(Status[] stats)
        {
            List<int> foundIds = new List<int>();

            foreach (Status status in stats)
            {
                foundIds.Add(status.playerId);
            }

            foreach (KeyValuePair<int, GameObject> keyValue in activePlayers)
            {
                if (!foundIds.Contains(keyValue.Key))
                {
                    keyValue.Value.SetActive(false);
                }
                else
                {
                    keyValue.Value.SetActive(true);
                }
            }
        }

        private IEnumerator CheckForLastUpdates()
        {
            WaitForSecondsRealtime delay = new WaitForSecondsRealtime(1);

            while (true)
            {
                foreach (PlayerManager playerManager in activePlayerManagers.Values)
                {
                    if ((DateTime.Now - playerManager.lastUpdated).TotalSeconds > 2)
                    {
                        playerManager.transform.parent.gameObject.SetActive(false);
                        playerManager.me.nameTag.SetActive(false);
                    }
                    else
                    {
                        playerManager.transform.parent.gameObject.SetActive(true);
                        if (playerManager.me && playerManager.me.nameTag)
                        {
                            playerManager.me.nameTag.SetActive(true);
                        }
                    }
                }

                yield return delay;
            }
        }

        public void PlayerQuit(int playerId)
        {
            Destroy(serverObjects[playerId].nameTag);
            Destroy(activePlayers[playerId]);

            serverObjects.Remove(playerId);
            activePlayers.Remove(playerId);
            activePlayerManagers.Remove(playerId);
        }

        public void OnDestroy()
        {
            
        }
    }
}