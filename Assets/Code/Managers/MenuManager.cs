﻿using System.Collections;
using Code.Networking;
using Shared.Packets;
using Code.Utility;
using LiteNetLib;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Managers
{
    public class MenuManager : MonoBehaviour
    {

        public Button playButton;
        public InputField ipInputField;
        public InputField nameInputField;

        public void Start()
        {
            playButton.interactable = true;
            playButton.onClick.AddListener(Connect);
        }

        private async void Connect()
        {
            LiteNetClient.Instance.Connect(ipInputField.text, nameInputField.text);
        }
    }
}
