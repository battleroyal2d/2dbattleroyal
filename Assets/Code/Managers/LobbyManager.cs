﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Code.Utility;
using Shared.Models;
using Code.Networking;
using Shared.Packets;
using LiteNetLib;

namespace Code.Managers
{
    public class LobbyManager : MonoBehaviour
    {
        public Text players;

        public Button ready;

        private bool readyState = false;

        public static bool startGame;

        public void Start()
        {
            ready.onClick.AddListener(ToggleReady);
            ready.GetComponentInChildren<Text>().text = "Ready";

        }

        public void Update()
        {
            List<Player> onlinePlayersRaw = DataManager.Instance.Get<List<Player>>("OnlinePlayers");
            Player player = DataManager.Instance.Get<Player>("Player");
            if (player != null)
            {
                players.text = player.name + "\n";
            }
            if (onlinePlayersRaw != null)
            {
                List<Player> onlinePlayers = new List<Player>(onlinePlayersRaw);
                foreach (Player onlinePlayer in onlinePlayers) {
                    players.text += onlinePlayer.name + "\n"; 
                }
            }
            if (startGame) 
            {
                ApplicationManager.Instance.switchScene(SceneList.GAME, SceneList.LOBBY);
                startGame = false;
            }
        }

        private void ToggleReady()
        {
            LiteNetClient.Instance.SendPacket(new ToggleReadyPacket(), DeliveryMethod.ReliableOrdered);
            readyState = !readyState;
            ready.GetComponentInChildren<Text>().text = readyState ? "Not Ready?" : "Ready";
        }
    }
}
