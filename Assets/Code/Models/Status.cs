﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteNetLib.Utils;

namespace Shared.Models
{
    public class Status
    {
        // Identifier
        public int playerId;

        // Position and Rotation
        public float x;
        public float y;

        public float c;
        public float w;

        // Everything else

        public EquipmentChanged equipmentChanged = new EquipmentChanged();
        
        public static void Serialize(NetDataWriter writer, Status status)
        { 
            writer.Put(status.playerId);
            writer.Put(status.x);
            writer.Put(status.y);
            writer.Put(status.c);
            writer.Put(status.w);
            EquipmentChanged.Serialize(writer, status.equipmentChanged);
        }

        public static Status Deserialize(NetDataReader reader)
        {
            Status status = new Status();
            
            status.playerId = reader.GetInt();
            status.x = reader.GetFloat();
            status.y = reader.GetFloat();
            status.c = reader.GetFloat();
            status.w = reader.GetFloat();
            status.equipmentChanged = EquipmentChanged.Deserialize(reader);

            return status;
        }
    }

    public class PickUp
    {
        public string itemId;

        public PickUp(string itemId)
        {
            this.itemId = itemId;
        }
    }
}
