using LiteNetLib.Utils;

namespace Shared.Models
{
    public class Player
    {
        public int id;
        public string name;
        public int health;
        public int shield;
        
        public static void Serialize(NetDataWriter writer, Player player)
        {
            writer.Put(player.id);
            writer.Put(player.name);
            writer.Put(player.health);
            writer.Put(player.shield);
        }

        public static Player Deserialize(NetDataReader reader)
        {
            Player player = new Player();
            
            player.id = reader.GetInt();
            player.name = reader.GetString();
            player.health = reader.GetInt();
            player.shield = reader.GetInt();
            
            return player;
        }
    }
}