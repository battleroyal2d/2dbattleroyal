﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteNetLib.Utils;
using UnityEngine;

namespace Shared.Models
{
    public class EquipmentChanged
    {
        public bool isDefault = true;
        public string head = "";
        public string chest = "";
        public string weapon = "";
        public string shield = "";
        public string feet = "";
        
        public static void Serialize(NetDataWriter writer, EquipmentChanged equipmentChanged)
        {
            writer.Put(equipmentChanged.head);
            writer.Put(equipmentChanged.chest);
            writer.Put(equipmentChanged.weapon);
            writer.Put(equipmentChanged.shield);
            writer.Put(equipmentChanged.feet);
        }

        public static EquipmentChanged Deserialize(NetDataReader reader)
        {
            EquipmentChanged equipmentChanged = new EquipmentChanged();
            
            equipmentChanged.head = reader.GetString();
            equipmentChanged.chest= reader.GetString();
            equipmentChanged.weapon = reader.GetString();
            equipmentChanged.shield = reader.GetString();
            equipmentChanged.feet = reader.GetString();

            return equipmentChanged;
        }
    }
}
