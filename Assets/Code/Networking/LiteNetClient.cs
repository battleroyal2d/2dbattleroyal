using System;
using System.Collections;
using System.Collections.Generic;
using Code.Managers;
using Shared.Models;
using Shared.Packets;
using Code.Utility;
using LiteNetLib;
using LiteNetLib.Utils;
using UnityEngine;

namespace Code.Networking
{
    public class LiteNetClient : Singleton<LiteNetClient>
    {
        private NetManager client;
        private EventBasedNetListener listener;
        private NetPacketProcessor packetProcessor;
        private NetDataWriter writer;
        private NetPeer server;

        private string username;

        public TickUpdateEvent TickUpdateEvent;
        
        public override void Awake()
        {
            base.Awake();

            if (TickUpdateEvent == null)
            {
                TickUpdateEvent = new TickUpdateEvent();
            }
            
            listener = new EventBasedNetListener();
            client = new NetManager(listener);
            packetProcessor = new NetPacketProcessor();
            writer = new NetDataWriter();
            client.Start();

            RegisterPackets();

            listener.PeerConnectedEvent += peer =>
            {
                server = peer;
                StartCoroutine(SendJoinPacket());
            };

            listener.PeerDisconnectedEvent += (peer, reason) =>
            {
                StartCoroutine(SwitchToStartMenuScene());
            };
            
            listener.NetworkReceiveEvent += (fromPeer, dataReader, deliveryMethod) =>
            {
                packetProcessor.ReadAllPackets(dataReader);
            };
        }

        public void Update()
        {
            if (client != null)
            {
                client.PollEvents();
            }
        }

        public void Connect(string ip, string username)
        {
            client.Connect(ip, 3000, "LiteNet");
            this.username = username;
        }

        public void SendPacket<T>(T packet, DeliveryMethod deliveryMethod) where T : class, new()
        {
            if (server == null) {
                return;
            }
            writer.Reset();
            packetProcessor.Write(writer, packet);
            server.Send(writer, deliveryMethod);
        }

        public void Dispose()
        {
            client.Stop();
        }
        
        private IEnumerator SendJoinPacket()
        {
            yield return new WaitForSeconds(1);
            
            SendPacket(new JoinPacket() { Name = username }, DeliveryMethod.ReliableOrdered);
        }
        
        private IEnumerator SwitchToStartMenuScene()
        {
            yield return new WaitForSeconds(1);
            
            SceneManagementManager.Instance.LoadLevel(SceneList.START_MENU, (t) =>
            {
                SceneManagementManager.Instance.UnLoadLevel(SceneList.GAME);
                Application.Quit();
            });
        }

        private void RegisterPackets()
        {
            packetProcessor.RegisterNestedType(Player.Serialize, Player.Deserialize);
            packetProcessor.RegisterNestedType(Status.Serialize, Status.Deserialize);
            packetProcessor.RegisterNestedType(EquipmentChanged.Serialize, EquipmentChanged.Deserialize);
            
            packetProcessor.SubscribeReusable<ConnectedPacket>(OnConnected);
            packetProcessor.SubscribeReusable<OnlinePlayersPacket>(OnOnlinePlayers);
            packetProcessor.SubscribeReusable<PlayerQuitPacket>(OnPlayerQuit);
            packetProcessor.SubscribeReusable<PlayerJoinedPacket>(OnPlayerJoined);
            packetProcessor.SubscribeReusable<StartGamePacket>(OnStartGame);
            packetProcessor.SubscribeReusable<StatusPacket>(OnStatus);
            packetProcessor.SubscribeReusable<PickUpPacket>(OnPickUp);
            packetProcessor.SubscribeReusable<EquipmentChangedPacket>(OnEquipmentChange);
            packetProcessor.SubscribeReusable<DamagePacket>(OnDamage);
            packetProcessor.SubscribeReusable<PlayerDeathPacket>(OnDeath);
            packetProcessor.SubscribeReusable<HazardZonePacket>(NewHazardZone);
            packetProcessor.SubscribeReusable<SpawnItemsPacket>(OnItemSpawn);

        }

        private void OnConnected(ConnectedPacket packet)
        {
            DataManager.Instance.Set("Player", packet.Player);
            ApplicationManager.Instance.switchScene(SceneList.LOBBY, SceneList.START_MENU);
        }
        
        private void OnOnlinePlayers(OnlinePlayersPacket packet)
        {
            List<Player> list = new List<Player>(packet.OnlinePlayers);
            DataManager.Instance.Set("OnlinePlayers", list);
        }
        
        private void OnPlayerQuit(PlayerQuitPacket packet)
        {
            List<Player> list = DataManager.Instance.Get<List<Player>>("OnlinePlayers");
            list.Remove(list.Find(x => x.id == packet.Player.id));
            DataManager.Instance.Set("OnlinePlayers", list);

            if (GameManager.Instance)
            {
                GameManager.Instance.playersToDelete.Enqueue(packet.Player.id);
            }
        }
        
        private void OnPlayerJoined(PlayerJoinedPacket packet)
        {
            List<Player> players = DataManager.Instance.Get<List<Player>>("OnlinePlayers");
            players.Add(packet.Player);
            DataManager.Instance.Set("OnlinePlayers", players);
        }
        
        private void OnStartGame(StartGamePacket packet)
        {
            LobbyManager.startGame = true;
        }
        
        private void OnStatus(StatusPacket packet)
        {
            TickUpdateEvent.Invoke(packet.Status);
        }
        
        private void OnPickUp(PickUpPacket packet)
        {
            GameManager.Instance.RemoveObject(packet);
        }

        private void OnEquipmentChange(EquipmentChangedPacket packet)
        {
            EquipmentChanged equipmentChanged = packet.EquipmentChanged;
            equipmentChanged.isDefault = false;
            Status equipmentStatus = new Status {equipmentChanged = equipmentChanged, playerId = packet.PlayerId};
            Status[] status = new Status[1];
            status[0] = equipmentStatus;
            TickUpdateEvent.Invoke(status);
        }

        private void OnDamage(DamagePacket packet)
        {
            PlayerStats localStats = GameObject.FindWithTag("LocalPlayer").GetComponent<PlayerStats>();
            localStats.TakeDamage(packet.DamageModifier);
        }

        private void OnDeath(PlayerDeathPacket packet)
        {
            // ignore everything hacky
        }

        private void NewHazardZone(HazardZonePacket packet)
        {
            HazardZone.Instance.SetTargetCircle(new Vector3(packet.PosX, packet.PosY), new Vector3(packet.Radius, packet.Radius), packet.ShrinkTimer);
        }

        private void OnItemSpawn(SpawnItemsPacket packet)
        {
            BoxItemSpawn box = DataManager.Instance.Get<BoxItemSpawn>(packet.BoxIdentifier);
            if (box != null)
            {
                box.SpawnItems(packet.Items);
            }
        }
    }
}