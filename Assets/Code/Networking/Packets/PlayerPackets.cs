using Shared.Models;

namespace Shared.Packets
{
    public class JoinPacket
    {
        public string Name { get; set; }
    }
    
    public class ConnectedPacket
    {
        public Player Player { get; set; }
    }

    public class PlayerJoinedPacket
    {
        public Player Player { get; set; }
    }
    
    public class PlayerQuitPacket
    {
        public Player Player { get; set; }
    }

    public class OnlinePlayersPacket 
    {
        public Player[] OnlinePlayers { get; set; }
    }
}