using Shared.Models;

namespace Shared.Packets
{
    public class ToggleReadyPacket
    {
        public byte Id { get; set; }
    }

    public class StartGamePacket
    {
        public byte Id { get; set; }
    }

    public class MovementPacket
    {
        public Status status { get; set; }
    }

    public class StatusPacket
    {
        public Status[] Status { get; set; }
    }

    public class PickUpPacket
    {
        public string ItemId { get; set; }
    }

    public class EquipmentChangedPacket
    {
        public int PlayerId { get; set; }
        public EquipmentChanged EquipmentChanged { get; set; }
    }

    public class AttackPlayerPacket
    {
        public int PlayerId { get; set; }
        public int KnockbackX { get; set; }
        public int KnockbackY { get; set; }
    }

    public class DamagePacket
    {
        public int DamageModifier { get; set; }
        public int KnockbackX { get; set; }
        public int KnockbackY { get; set; }
        public bool Heal { get; set; }
    }

    public class PlayerDeathPacket
    {
        public int PlayerId { get; set; }
    }

    public class HazardZonePacket
    {
        public float PosX { get; set; }
        public float PosY { get; set; }
        public int Radius { get; set; }
        public float ShrinkTimer { get; set; }
    }

    public class BoxClickedPacket
    {
        public string BoxIdentifier { get; set; }
    }
    
    public class SpawnItemsPacket
    {
        public string BoxIdentifier { get; set; }
        public string[] Items { get; set; }
    }
}