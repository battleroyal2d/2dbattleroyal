﻿// using System;
// using System.Collections.Generic;
// using System.Threading.Tasks;
// using Shared.Models;
// using Code.Utility;
// using Newtonsoft.Json;
// using SocketIOClient;
// using UnityEngine;
// using Code.Managers;
//
// namespace Code.Networking
// {
//     public class NetworkClient : Singleton<NetworkClient>
//     {
//         private SocketIO client;
//         public TickUpdateEvent TickUpdate;
//
//         public Task Initialize(string url)
//         {
//             if (TickUpdate == null) TickUpdate = new TickUpdateEvent();
//
//             client = new SocketIO($"ws://{url}:8084");
//             client.OnClosed += async reason =>
//             {
//                 if (reason == ServerCloseReason.Aborted)
//                 {
//                     Debug.LogError("Connection to server was aborted!");
//                 }
//             };
//             SetupEvents();
//             return client.ConnectAsync();
//         }
//
//         private void SetupEvents()
//         {
//             RegisterEventListener<Player>("connected", data =>
//             {
//                 DataManager.Instance.Set("Player", data);
//             });
//
//             RegisterEventListener<Player[]>("online-players", data =>
//             {
//                 List<Player> list = new List<Player>(data);
//                 DataManager.Instance.Set("OnlinePlayers", list);
//             });
//
//             RegisterEventListener<Player>("player-quit", data =>
//             {
//                 List<Player> list = DataManager.Instance.Get<List<Player>>("OnlinePlayers");
//                 list.Remove(list.Find(x => x.id == data.id));
//                 DataManager.Instance.Set("OnlinePlayers", list);
//
//                 if (GameManager.Instance)
//                 {
//                     GameManager.Instance.playersToDelete.Enqueue(data.id);
//                 }
//             });
//
//             RegisterEventListener<Player>("player-joined", data =>
//             {
//                 List<Player> players = DataManager.Instance.Get<List<Player>>("OnlinePlayers");
//                 players.Add(data);
//                 DataManager.Instance.Set("OnlinePlayers", players);
//             });
//             
//             RegisterEventListener<object>("start-game", data =>
//             {
//                 LobbyManager.startGame = true;
//             });
//             RegisterEventListener<Status[]>("status", data =>
//             {
//                 this.TickUpdate.Invoke(data);
//                 GameManager.Instance.HidePlayerIfNeeded(data);
//             });
//             RegisterEventListener<PickUp>("item-pick-up", data =>
//             {
//                 Debug.Log("In network Client");
//                 GameManager.Instance.RemoveObject(data);
//             });
//         }
//
//         public void Emit(string eventName, object data = null)
//         {
//             client.EmitAsync(eventName, data);
//         }
//
//         public void RegisterEventListener<T>(string eventName, Action<T> callback)
//         {
//             client.On(eventName, res =>
//             {
//                 var data = Deserialize<T>(res.Text);
//                 callback(data);
//             });
//         }
//
//         private T Deserialize<T>(string json)
//         {
//             return JsonConvert.DeserializeObject<T>(json);
//         }
//
//         public async void Dispose()
//         {
//             if (client != null)
//             {
//                 await client.CloseAsync();
//             }
//         }
//     }
// }