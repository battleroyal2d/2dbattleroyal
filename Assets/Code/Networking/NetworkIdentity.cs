﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Code.Managers;
using Code.Utility;
using Shared.Models;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.UI;

namespace Code.Networking
{
    public class NetworkIdentity : MonoBehaviour
    {
        public int id;
        public bool isControlling;

        public GameObject nameTagPrefab;

        public GameObject nameTag;

        public GearSocket helmet;
        public GearSocket shield;
        public GearSocket weapon;

        public void SetControllerID(Player myPlayer)
        {
            this.id = myPlayer.id;
            createNameTag(myPlayer);
            Player player = DataManager.Instance.Get<Player>("Player");
            this.isControlling = player.id == this.id;
        }

        private void Start()
        {
            UnequipItems();
        }

        void Update()
        {
            nameTag.transform.SetPositionAndRotation(
                transform.position + new Vector3(0, 30, 0),
                new Quaternion(0, 0, 0, 0)
            );
        }

        public void UpdateEquipment(EquipmentChanged equipmentChanged, int playerId)
        {
            if (id != playerId)
            {
                return;
            }


            if (equipmentChanged.head == "" && helmet.isEquiped)
            {
                helmet.Dequip();
            }

            if (equipmentChanged.head != "" && !helmet.isEquiped)
            {
                helmet.Equip();
            }

            if (equipmentChanged.shield == "" && shield.isEquiped)
            {
                shield.Dequip();
            }

            if (equipmentChanged.shield != "" && !shield.isEquiped)
            {
                shield.Equip();
            }

            if (equipmentChanged.weapon == "" && weapon.isEquiped)
            {
                weapon.Dequip();
            }

            if (equipmentChanged.weapon != "" && !weapon.isEquiped)
            {
                weapon.Equip();
            }
        }

        private void createNameTag(Player player)
        {
            Transform nameTagContainer = GameObject.FindWithTag("NameTagContainer").transform;

            nameTag = Instantiate(nameTagPrefab, nameTagContainer);
            nameTag.SetActive(true);
            nameTag.name = player.name;
            nameTag.GetComponentInChildren<Text>().text = player.name;
            nameTag.transform.position = transform.position + new Vector3(0, 30, 0);
        }

        private void UnequipItems()
        {
            helmet.Dequip();
            shield.Dequip();
            weapon.Dequip();
        }
    }
}