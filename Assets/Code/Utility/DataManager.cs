﻿using System.Collections.Concurrent;
using UnityEngine;

namespace Code.Utility
{
    public class DataManager : Singleton<DataManager>
    {
        private ConcurrentDictionary<string, object> storage;

        public override void Awake()
        {
            base.Awake();
            this.storage = new ConcurrentDictionary<string, object>();
        }

        public T Get<T>(string key)
        {
            this.storage.TryGetValue(key, out var returnValue);
            return (T) returnValue;
        }

        public void Set(string key, object value)
        {
            this.storage[key] = value;
        }

        public void Clear()
        {
            this.storage.Clear();
        }
    }
}
