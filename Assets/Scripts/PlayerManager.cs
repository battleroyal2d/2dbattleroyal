using System;
using System.Collections;
using System.Collections.Generic;
using Code.Networking;
using UnityEngine;

using Code.Managers;
using Code.Utility;
using Shared.Models;
using System.Threading.Tasks;
using System.Threading;
using Shared.Packets;
using LiteNetLib;
using Newtonsoft.Json;

public class PlayerManager : MonoBehaviour
{
    [Header("Data")]
    [SerializeField]
    private float speed = 4;
    public Interactable focus;
    Camera cam;
    public GameObject player;
    public CharacterStats myStats;
    public float radius = 65;
    public Transform interactionTransform;
    public LayerMask whatIsHazardZone;
    public Rigidbody2D rgbd;

    public NetworkIdentity me;
    public Status status = new Status();

    public bool isControlling = false;

    public DateTime lastUpdated = DateTime.Now;
    private Status lastStatus;
    private EquipmentChanged currentEquipment;

    #region Movement
    void Start()
    {
        LiteNetClient.Instance.TickUpdateEvent.AddListener(UpdateStatus);
        this.me = GetComponent<NetworkIdentity>();
        this.isControlling = me.isControlling;
        cam = Camera.main;
        rgbd = GetComponent<Rigidbody2D>();
        myStats = player.GetComponent<CharacterStats>();
        InvokeRepeating("checkIfInsideHazardArea", 2.0f, 1.0f);
    }

    public void StartTickUpdate()
    {
        if (isControlling)
        {
            StartCoroutine(SendPosition());
        }
    }

    private void Update()
    {
        if (!isControlling)
        {
            transform.SetPositionAndRotation(
                new Vector3(this.status.x, this.status.y, 0),
                new Quaternion(0, 0, this.status.c, this.status.w)
            );
            return;
        }
        
        // For picking up Items (Maybe switch it with a KeyPress?)
        // Press right mouseButton
        if (Input.GetMouseButtonDown(1))
        {
            Vector3 pointedLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pointedLocation.z = 10;
            RaycastHit2D hit = Physics2D.Raycast(pointedLocation, -Vector2.up);

            if (hit)
            {
                Interactable interactable = hit.collider.GetComponent<Interactable>();
              
                
                if (interactable != null)
                {
                   // Attack enemy
                   if (interactable.isInDistance(transform))
                   {
                    this.setFocus(interactable);
                    
                    if (interactable is ItemPickup pickup)
                    {
                        Debug.Log(pickup.UniqueHash);
                        LiteNetClient.Instance.SendPacket(new PickUpPacket() { ItemId = pickup.UniqueHash}, DeliveryMethod.ReliableOrdered);
                    }      
                   }
                }
            }
        }
    }

    public void UpdateStatus(Status[] stats)
    {
        lastUpdated = DateTime.Now;
        int id = me.id;
        foreach (Status stat in stats)
        {
            if (stat.playerId == id)
            {
                this.status = stat;
                if (!stat.equipmentChanged.isDefault)
                {
                    Debug.Log("UpdateEquipment");
                    me.UpdateEquipment(stat.equipmentChanged, stat.playerId);
                }
            }
        }
    }

    // todo: Method has to be replaced if networking is finished
    public void FixedUpdate()
    {
        if (!isControlling)
        {
            return;
        }
        this.checkMovement();
        this.checkAiming();
    }

    private void checkIfInsideHazardArea()
    {
        if (HazardZone.isOutsideCircle_Static(transform.position) && this.isControlling)
        {
            myStats.TakeDamage(5);
            LiteNetClient.Instance.SendPacket(new DamagePacket() { DamageModifier = 5, KnockbackX = 0, KnockbackY = 0}, DeliveryMethod.ReliableOrdered);
        }
    }



    private void checkMovement()
    {
        
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");
        rgbd.velocity = new Vector2(horizontal * speed, vertical * speed);
            //transform.position += new Vector3(horizontal, vertical, 0) * speed;
      
    }

    private void checkAiming()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 dif = mousePosition - transform.position;
        dif.Normalize();
        float rotation = Mathf.Atan2(dif.y, dif.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, rotation - 90);

    }

    private void setFocus(Interactable newFocus)
    {
        if (newFocus != focus)
        {
            if (focus != null)
            {
                focus.onDefocused();
            }
            focus = newFocus;
        }
        newFocus.onFocused(transform);
    }

    private void removeFocus()
    {
        if (focus != null)
        {
            focus.onDefocused();
        }
        focus = null;
    }

    public IEnumerator SendPosition()
    {
        WaitForSecondsRealtime delay = new WaitForSecondsRealtime(0.021f);

        while (true)
        {
            Player player = DataManager.Instance.Get<Player>("Player");

            Status stat = new Status();

            stat.playerId = player.id;

            stat.x = transform.position.x;
            stat.y = transform.position.y;

            stat.c = transform.rotation.z;
            stat.w = transform.rotation.w;

            if (stat != lastStatus)
            {
                lastStatus = stat;
                LiteNetClient.Instance.SendPacket(new MovementPacket() { status = stat}, DeliveryMethod.ReliableSequenced);
            }


            yield return delay;
        }
    }

    #endregion
    
}

