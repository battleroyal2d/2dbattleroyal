﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

[System.Serializable]
public class Stat : MonoBehaviour
{
    public static Stat instance;

    #region instance
    void Awake()
    {
        instance = this;
    }
    #endregion
    private Image content;

    
    public Text statValueText;

    private List<float> modifiers = new List<float>();
    private float finalValue;
    private float currentFill;
    public float MyMaxValue { get; set; }
    private float currentValue;
    public float MyCurrentValue
    {
        get
        {
            return currentValue;
        }
        set
        {
            if (value > MyMaxValue)
            {
                currentValue = MyMaxValue;
            } else if (value < 0 )
            {
                currentValue = 0;
            } else
            {
                currentValue = value;
            }
            currentFill = currentValue / MyMaxValue;
            statValueText.text = currentValue + " / " + MyMaxValue;
        }
    }

    public void AddModifier(float modifier)
    {
        if (modifier != 0)
        {
            finalValue = 0;
            modifiers.Add(modifier);
            modifiers.ForEach(x => finalValue += x);
            MyCurrentValue = finalValue;
        }
        
    }

    public void RemoveModifier(float modifier)
    {
        if (modifier != 0)
        {
           
            finalValue = 0;
            modifiers.Remove(modifier);
            modifiers.ForEach(x => finalValue += x);
            MyCurrentValue = finalValue;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        content = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        content.fillAmount = currentFill;
    }

    public void Initialize(int currentValue, int maxValue)
    {
        MyMaxValue = maxValue;
        MyCurrentValue = currentValue;
    }
}
