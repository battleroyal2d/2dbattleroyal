﻿using UnityEngine;
using Code.Networking;
using Shared.Packets;

public class CharacterStats : MonoBehaviour
{
    [SerializeField]
    public Stat health;
    [SerializeField]
    public Stat armor;
    [SerializeField]
    public Stat myDamage;
    private float switchValue = 0;
    private GameObject objToSpawn;
    public Sprite graveSprite;
    public Equipment[] equipment;


    private void Awake()
    {

    }

    private void Start()
    {
        int numberOfSlots = System.Enum.GetNames(typeof(EquipmentSlot)).Length + 1;
        equipment = new Equipment[numberOfSlots];
    }

    // For Debug
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            TakeDamage(2);
        }
    }

    public void Heal(float heal)
    {
        LiteNetClient.Instance.SendPacket(new DamagePacket() { DamageModifier = (int)heal, KnockbackX = 0, KnockbackY = 0, Heal = true }, LiteNetLib.DeliveryMethod.ReliableOrdered);
        health.MyCurrentValue += heal;
    }


    public void TakeDamage(float damage)
    {
        damage = Mathf.Clamp(damage, 0, int.MaxValue);


        // if player has armor
        if (armor.MyCurrentValue != 0)
        {
            if (damage > armor.MyCurrentValue)
            {
                switchValue = armor.MyCurrentValue;
                armor.MyCurrentValue -= damage;
                damage -= switchValue;
                health.MyCurrentValue -= damage;
               
            }
            else
            {
                armor.MyCurrentValue -= damage;
               
            }
            destroyArmor(damage, 0);
        }
        else
        {
            health.MyCurrentValue -= damage;
        }


        if (health.MyCurrentValue == 0)
        {
            Die();
        }
    }

    public virtual void Die()
    {
        // Die
        spawnGrave();
        GameObject playerThatDies = GetComponent<PlayerManager>().gameObject;
        Destroy(playerThatDies);
    }

    private void spawnGrave()
    {
        // Handle the spawn of a box
        objToSpawn = new GameObject("Grave");
        objToSpawn.AddComponent<SpriteRenderer>();
        objToSpawn.GetComponent<SpriteRenderer>().sprite = graveSprite;
        objToSpawn.transform.position = this.transform.position;
        objToSpawn.transform.localScale = new Vector3(100, 100, 1);
        objToSpawn.GetComponent<SpriteRenderer>().sortingLayerName = "Decorations above player";
    }

    private void destroyArmor(float damage, int pos)
    {
        equipment = CharacterPanel.instance.currentEquipment;
        for (int i = pos; i < equipment.Length; i++)
        {
            float armorModifier = 0;
           
           
            if (equipment[i] != null)
            {

                if (equipment[i].armorModifier > 0)
                {
                    armorModifier = equipment[i].armorModifier;
                    float saveDamage = damage;

                    // if damage is higher than the equipments armor modifier
                    if (damage == equipment[i].armorModifier)
                    {
                        equipment[i].armorModifier = 0;
                        armor.RemoveModifier(damage);
                        break;
                       
                    } else
                    {
                        if (damage > equipment[i].armorModifier)
                        {
                            switchValue = equipment[i].armorModifier;
                            equipment[i].armorModifier = 0;
                            damage -= switchValue;
                            armor.RemoveModifier(armorModifier);
                        }
                        else
                        {
                            equipment[i].armorModifier -= damage;
                            armor.RemoveModifier(armorModifier);
                            armor.AddModifier(armorModifier - damage);
                            break;
                        }
                    }
                 
                }
            }
        }
    }
}
