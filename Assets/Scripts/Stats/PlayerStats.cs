﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : CharacterStats
{
    #region Singleton
    public static PlayerStats instance;
    void Awake()
    {
        if (instance != null)
        {
            return;
        }
        instance = this;
    }
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        health.Initialize(100, 100);
        armor.Initialize(0, 100);
        myDamage.Initialize(0,100);
        EquipmentManager.Instance.onEquipmentChanged += OnEquipmentChanged;
    }

    void OnEquipmentChanged(Equipment newItem, Equipment oldItem)
    {
        
            if (newItem != null)
            {
                if ((int)newItem.equipmentSlot == 2)
                {
                    myDamage.AddModifier(newItem.damageModifier);
                } else {
                    armor.AddModifier(newItem.armorModifier);
                }
            }
            if (oldItem != null)
            {
                armor.RemoveModifier(oldItem.armorModifier);
            }
        }

    private void HealUsed(Useables item)
    {
        if (item != null)
        {
            health.MyCurrentValue += item.healthModifier;
        }
    }
}
