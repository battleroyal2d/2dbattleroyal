﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Code.Networking;
using Code.Utility;
using Shared.Models;

public class EquipmentManager : Singleton<EquipmentManager>
{
    CharacterPanel characterPanel;
    Inventory inventory;
    Equipment[] currentEquipment;
    public delegate void OnEquipmentChanged(Equipment newItem, Equipment oldItem);
    public OnEquipmentChanged onEquipmentChanged;
    // Start is called before the first frame update
    void Start()
    {
        characterPanel = CharacterPanel.instance;
        inventory = Inventory.instance;
        int numberOfSlots = System.Enum.GetNames(typeof(EquipmentSlot)).Length + 1;
        currentEquipment = new Equipment[numberOfSlots];
    }

    public void Equip(Equipment newItem)
    {
        int slotIndex = (int)newItem.equipmentSlot;
        Equipment oldItem = null;
        if (currentEquipment[slotIndex] != null)
        {
            oldItem = currentEquipment[slotIndex];
        }

        if (onEquipmentChanged != null)
        {
            onEquipmentChanged.Invoke(newItem, oldItem);
        }
        currentEquipment[slotIndex] = newItem;
        characterPanel.Add(newItem);

//        EquipmentChanged equip = new EquipmentChanged();
//
//        foreach (Equipment q in currentEquipment)
//        {
//            switch ((int) q.equipmentSlot)
//            {
//                case 0:
//                    equip.head = q.ID;
//                    break;
//                case 1:
//                    equip.chest = q.ID;
//                    break;
//                case 2:
//                    equip.weapon = q.ID;
//                    break;
//                case 3:
//                    equip.shield = q.ID;
//                    break;
//                case 4:
//                    equip.feet = q.ID;
//                    break;
//            }
//        }
//
//        NetworkClient.Instance.Emit("equipment-changed", equip);

    }

    public void Unequip(int slotIndex)
    {
        if (currentEquipment[slotIndex] != null)
        {
            Equipment oldItem = currentEquipment[slotIndex];
            currentEquipment[slotIndex] = null;
            inventory.Add(oldItem);
            oldItem.equiped = false;
            if (onEquipmentChanged != null)
            {
                onEquipmentChanged.Invoke(null, oldItem);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
