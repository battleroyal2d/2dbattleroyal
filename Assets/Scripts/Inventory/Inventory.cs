﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public static Inventory instance;
    public Items newItem;

    #region Singleton
    void Awake ()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found!");
        } 
        instance = this;
    }
    #endregion

    // Callback which is triggered when
    // an item gets added/removed.
    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public int space = 9;	// Amount of slots in inventory

    public List<Items> items = new List<Items>();
    private static GameObject playerRef;

    private void Start()
    {
        playerRef = GameObject.Find("Player-Prefab");
    }
    public bool Add (Items item)
    {
        if (!item.isDefaultItem)
        {
            if (items.Count >= space)
            {
                return false;
            } 
            items.Add(item);

            // Tell that something changed
            if (onItemChangedCallback != null)
            {
                onItemChangedCallback.Invoke();
            }
        }
        return true;
    }

    public void Remove (Items item)
    {
        
        // todo: create new instance of dropped Item and spawn it at user
        // then remove the item
        items.Remove(item);
        // Tell that something changed
        if (onItemChangedCallback != null)
        {
            onItemChangedCallback.Invoke();
        }
    }
}
