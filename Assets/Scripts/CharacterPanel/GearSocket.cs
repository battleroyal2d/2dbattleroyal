﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearSocket : MonoBehaviour
{
    public bool isEquiped = false;
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Equip()
    {
        isEquiped = true;
        spriteRenderer.color = Color.white;
    }

    public void Dequip()
    {
        isEquiped = false;
        Color c = spriteRenderer.color;
        c.a = 0;
        spriteRenderer.color = c;
    }
}
