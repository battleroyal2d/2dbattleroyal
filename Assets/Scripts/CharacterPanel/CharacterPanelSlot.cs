﻿using Code.Networking;
using UnityEngine;
using UnityEngine.UI;

public class CharacterPanelSlot : MonoBehaviour
{
    public Image icon;
    Equipment equipment;
    public Button removeButton;
    [SerializeField]
    private GearSocket gearSocket;

    public void AddItem(Equipment newItem)
    {
        equipment = newItem;
        icon.sprite = equipment.icon;
        icon.enabled = true;
        removeButton.interactable = true;

        if (gearSocket != null)
        {
            gearSocket.Equip();
        }
    }

    public void ClearSlot()
    {
        equipment = null;
        icon.sprite = null;
        icon.enabled = false;
        removeButton.interactable = false;
        if (gearSocket != null)
        {
            gearSocket.Dequip();
        }
    }

    public void onRemoveButton()
    {
        CharacterPanel.instance.Unequip(equipment);
    }

    public void UseItem()
    {
        if (equipment != null)
        {
            equipment.Use();
        }
    }
}
