﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterPanel : MonoBehaviour
{

    #region Singleton
    public static CharacterPanel instance;
    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found!");
        }
        instance = this;
    }
    #endregion

    public int space = 6;	// Amount of slots in characterPanel
    public Equipment[] currentEquipment;
    public delegate void OnEquipmentChanged();
    public OnEquipmentChanged onEquipmentChangedCallback;
    Inventory inventory;
    // Start is called before the first frame update
    void Start()
    {
        int numberOfSlots = System.Enum.GetNames(typeof(EquipmentSlot)).Length + 1;
        currentEquipment = new Equipment[numberOfSlots];

        inventory = Inventory.instance;
    }
    public void Update()
    {
        
    }

    public bool Add(Equipment equipment)
    {
        int slotIndex = (int)equipment.equipmentSlot;
        if (!equipment.isDefaultItem)
        {
            if (currentEquipment[slotIndex] != null)
            {
                return false;
            }
            currentEquipment[slotIndex] = equipment;
            equipment.equiped = true;
            
           
            // Tell that something changed
            if (onEquipmentChangedCallback != null)
            {
                onEquipmentChangedCallback.Invoke();
            }
           
        }
        return true;
    }

    public void Unequip(Equipment equipment)
    {
       
        int slotIndex = (int)equipment.equipmentSlot;
        EquipmentManager.Instance.Unequip(slotIndex);
        currentEquipment[slotIndex] = null;
        // Tell that something changed
        if (onEquipmentChangedCallback != null)
        {
            onEquipmentChangedCallback.Invoke();
        }
    }
}
