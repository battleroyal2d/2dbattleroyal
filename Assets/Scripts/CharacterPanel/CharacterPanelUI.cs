﻿using System;
using System.Collections;
using System.Collections.Generic;
using Shared.Models;
using Code.Networking;
using Shared.Packets;
using LiteNetLib;
using UnityEngine;

public class CharacterPanelUI : MonoBehaviour
{
    public Transform itemsParent;
    public GameObject characterPanelUI;	// The entire UI
    CharacterPanel characterPanel;
    CharacterPanelSlot[] slots;
    
    private bool sendEquipment = false;
    
    // Start is called before the first frame update
    void Start()
    {
        characterPanel = CharacterPanel.instance;
        // subscribe on onItemChangedCallback, to check if Inventory changed
        characterPanel.onEquipmentChangedCallback += UpdateUi;

        slots = itemsParent.GetComponentsInChildren<CharacterPanelSlot>();
    }

    void UpdateUi()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (characterPanel.currentEquipment[i] != null)
            {
                slots[i].AddItem(characterPanel.currentEquipment[i]);
            }
            else
            {
                slots[i].ClearSlot();
            }
        }

        sendEquipment = true;
    }
    

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("CharacterPanel"))
        {
            characterPanelUI.SetActive(!characterPanelUI.activeSelf);
        }


        if (sendEquipment)
        {
            EquipmentChanged equipmentChanged = new EquipmentChanged();

            foreach (Equipment equipment in characterPanel.currentEquipment)
            {
                if (equipment != null)
                {
                    switch (equipment.equipmentSlot)
                    {
                        case EquipmentSlot.Chest:
                            equipmentChanged.chest = equipment.name + '#' + equipment.armorModifier;
                            break;
                        case EquipmentSlot.Head:
                            equipmentChanged.head = equipment.name + '#' + equipment.armorModifier;
                            break;
                        case EquipmentSlot.Weapon:
                            equipmentChanged.weapon = equipment.name;
                            break;
                        case EquipmentSlot.Shield:
                            equipmentChanged.shield = equipment.name + '#' + equipment.armorModifier;
                            break;
                        case EquipmentSlot.Feet:
                            equipmentChanged.feet = equipment.name + '#' + equipment.armorModifier;
                            break;
                    }
                }
            }

            LiteNetClient.Instance.SendPacket(new EquipmentChangedPacket() { EquipmentChanged = equipmentChanged}, DeliveryMethod.ReliableOrdered);
            sendEquipment = false;
        }
    }
}
