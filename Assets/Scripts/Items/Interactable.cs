﻿using UnityEngine;

public class Interactable : MonoBehaviour
{
    public float radius = 3f;
    bool isFocus = false;
    Transform player;
    public Transform interactionTransform;

    bool hasInteracted = false;

    void Update()
    {
        if (isFocus && !hasInteracted)
        {
            if (isInDistance(player))
            {
                hasInteracted = true;
                Interact();
            }
        }
    }

    public bool isInDistance(Transform playerTransform)
    {
        player = playerTransform;
        float distance = Vector2.Distance(player.position, transform.position);

        return distance <= radius;
    }

    public virtual void Interact()
    {
        
    }

    public void onFocused(Transform playerTransform)
    {
        isFocus = true;
        player = playerTransform;
        hasInteracted = false;
    }

    public void onDefocused ()
    {
        isFocus = false;
        player = null;
        hasInteracted = false;
    }

    void OnDrawGizmosSelected()
    {
        if (interactionTransform == null)
        {
            interactionTransform = transform;
        }
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
