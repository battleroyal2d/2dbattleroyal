﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Useable", menuName = "Inventory/Useable")]
public class Useables : Items
{
    public float healthModifier;
    
    public override void Use()
    {
        base.Use();
        PlayerStats.instance.Heal(healthModifier);
        RemoveFromInventory();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
