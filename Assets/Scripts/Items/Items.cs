﻿using UnityEngine;
using System;

/* The base item class. All items should derive from this. */

[CreateAssetMenu(fileName = "New", menuName = "Inventory/Item")]
public class Items : ScriptableObject
{
    new public string name = "New Item";    // Name of the item
    public Sprite icon = null;              // Item icon
    public bool showInInventory = true;
    public bool isDefaultItem = false;
    public bool equiped = false;

    // Called when the item is pressed in the inventory
    public virtual void Use()
    {
        // Use the item
        // Something may happen
    }

    // Call this method to remove the item from inventory
    public void RemoveFromInventory()
    {
        Inventory.instance.Remove(this);
    }
}