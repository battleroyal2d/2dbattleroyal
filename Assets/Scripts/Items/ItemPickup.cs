﻿using System;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public class ItemPickup : Interactable
{
    public Items item;	// Item to put in the inventory on pickup

    public string UniqueHash;

    public void Start()
    {
        var position = transform.position;
        string hash = position.x + "#" + position.y + "#" + item.name;
        UniqueHash = hash.Replace(" ", string.Empty).Replace(",", ".");
    }

    public override void Interact()
    {
        base.Interact();
        PickUp();
    }

    public void PickUp()
    {
        bool wasPickedUp = Inventory.instance.Add(item);    // Add to inventory

        if (wasPickedUp)
        {
            Destroy(gameObject);    // Destroy item from scene
        }
    }
}
