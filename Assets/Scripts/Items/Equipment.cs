﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "new Equipment", menuName = "Inventory/Equipment")]
public class Equipment : Items
{
    public EquipmentSlot equipmentSlot;
    public float armorModifier;
    public float damageModifier;
    

    public void Awake()
    {
        equiped = false;
    }

    public void OnEnable()
    {
        equiped = false;
    }

    public void OnDestroy()
    {
        equiped = false;
    }

    public override void Use()
    {
        if (equiped == false)
        {
            base.Use();
            EquipmentManager.Instance.Equip(this);
            RemoveFromInventory();
        }
            
    }
}
public enum EquipmentSlot
{
    Head, Chest, Weapon, Shield, Feet,
} 