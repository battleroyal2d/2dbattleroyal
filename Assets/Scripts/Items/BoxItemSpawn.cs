﻿using System;
using System.Collections;
using System.Collections.Generic;
using Code.Networking;
using Code.Utility;
using LiteNetLib;
using Shared.Packets;
using UnityEngine;
using UnityEditor;

public class BoxItemSpawn : Interactable
{
    private GameObject objToSpawn;
    public Sprite shieldSprite;
    public Sprite helmetSprite;
    public Sprite chestSprite;
    public Sprite swordSprite;
    public Sprite feetSprite;
    public Sprite healingPotionSprite;
    public string[] items = new string[4];
    
    public string identifier;

    private void Awake()
    {
        var position = transform.position;
        identifier = "Box#" + position.x + position.y + position.z;
        identifier = identifier.Replace(",", ".");
        DataManager.Instance.Set(identifier, this);
    }

    public override void Interact()
    {
        base.Interact();
        
        LiteNetClient.Instance.SendPacket(new BoxClickedPacket() { BoxIdentifier = identifier }, DeliveryMethod.ReliableOrdered);
    }

    public void SpawnItems(string[] itemToSpawn)
    {
        Equipment newEquipmentObject;
        for (int i = 0; i < itemToSpawn.Length; i++)
        {
            switch (itemToSpawn[i])
            {
                case "Helmet of Protection":
                    initializeSameComponentsForItems(itemToSpawn[i], i);
                    newEquipmentObject = createNewScripableObjectEquipment(itemToSpawn[i]);
                    objToSpawn.GetComponent<ItemPickup>().item = newEquipmentObject;
                    newEquipmentObject.damageModifier = 0;
                    newEquipmentObject.armorModifier = 10;
                    newEquipmentObject.equipmentSlot = EquipmentSlot.Head;
                    newEquipmentObject.icon = helmetSprite;
                    objToSpawn.GetComponent<SpriteRenderer>().sprite = helmetSprite;
                    break;
                case "Chest of Protection":
                    initializeSameComponentsForItems(itemToSpawn[i], i);
                    newEquipmentObject = createNewScripableObjectEquipment(itemToSpawn[i]);
                    newEquipmentObject.damageModifier = 0;
                    newEquipmentObject.armorModifier = 10;
                    newEquipmentObject.equipmentSlot = EquipmentSlot.Chest;
                    newEquipmentObject.icon = chestSprite;
                    objToSpawn.GetComponent<ItemPickup>().item = newEquipmentObject;
                    objToSpawn.GetComponent<SpriteRenderer>().sprite = chestSprite;
                    break;
                case "Sword of Doom":
                    initializeSameComponentsForItems(itemToSpawn[i], i);
                    newEquipmentObject = createNewScripableObjectEquipment(itemToSpawn[i]);
                    newEquipmentObject.damageModifier = 20;
                    newEquipmentObject.armorModifier = 0;
                    newEquipmentObject.equipmentSlot = EquipmentSlot.Weapon;
                    newEquipmentObject.icon = swordSprite;
                    objToSpawn.GetComponent<ItemPickup>().item = newEquipmentObject;
                    objToSpawn.GetComponent<SpriteRenderer>().sprite = swordSprite;
                    break;
                case "Shield of Protection":
                    initializeSameComponentsForItems(itemToSpawn[i], i);
                    newEquipmentObject = createNewScripableObjectEquipment(itemToSpawn[i]);
                    newEquipmentObject.damageModifier = 0;
                    newEquipmentObject.armorModifier = 20;
                    newEquipmentObject.equipmentSlot = EquipmentSlot.Shield;
                    newEquipmentObject.icon = shieldSprite;
                    objToSpawn.GetComponent<ItemPickup>().item = newEquipmentObject;
                    objToSpawn.GetComponent<SpriteRenderer>().sprite = shieldSprite;
                    break;
                case "Feet of Protection":
                    initializeSameComponentsForItems(itemToSpawn[i], i);
                    newEquipmentObject = createNewScripableObjectEquipment(itemToSpawn[i]);
                    newEquipmentObject.damageModifier = 0;
                    newEquipmentObject.armorModifier = 5;
                    newEquipmentObject.equipmentSlot = EquipmentSlot.Feet;
                    newEquipmentObject.icon = feetSprite;
                    objToSpawn.GetComponent<ItemPickup>().item = newEquipmentObject;
                    objToSpawn.GetComponent<SpriteRenderer>().sprite = feetSprite;
                    break;
                case "Healing Potion":
                    initializeSameComponentsForItems(itemToSpawn[i], i);
                    objToSpawn.GetComponent<ItemPickup>().item = createNewScripableObjectUseables(itemToSpawn[i]);
                    objToSpawn.GetComponent<SpriteRenderer>().sprite = healingPotionSprite;
                    break;
            }
        }
        GameObject boxThatDisappears = this.gameObject;
        Destroy(boxThatDisappears);
    }

    public Equipment createNewScripableObjectEquipment(string itemName)
    {
        ScriptableObject.CreateInstance<Items>();
        Equipment asset = ScriptableObject.CreateInstance<Equipment>();
        asset.name = itemName;
        var position = this.transform.position;
        AssetDatabase.CreateAsset(asset, $"Assets/Items/{itemName}-{position.x + position.y + position.z}.asset");
        AssetDatabase.SaveAssets();
        return asset;
    }

    public Useables createNewScripableObjectUseables(string itemName)
    {
        ScriptableObject.CreateInstance<Items>();
        Useables asset = ScriptableObject.CreateInstance<Useables>();
        var position = this.transform.position;
        AssetDatabase.CreateAsset(asset, $"Assets/Items/{itemName}-{position.x + position.y + position.z}.asset");
        asset.healthModifier = 10;
        asset.name = itemName;
        asset.icon = healingPotionSprite;
        AssetDatabase.SaveAssets();
        return asset;
    }

    public void initializeSameComponentsForItems(string item, int count)
    {
        Vector3 newSpawnPosition;
        if (count == 0)
        {
            newSpawnPosition = new Vector3(transform.position.x - 50, transform.position.y, transform.position.z);
        } else if (count == 1)
        {
            newSpawnPosition = new Vector3(transform.position.x + 50, transform.position.y, transform.position.z);
        } else if (count == 2)
        {
            newSpawnPosition = new Vector3(transform.position.x, transform.position.y - 50, transform.position.z);
        } else
        {
            newSpawnPosition = new Vector3(transform.position.x , transform.position.y + 50, transform.position.z);
        }
        objToSpawn = new GameObject();
        objToSpawn.name = item;
        objToSpawn.AddComponent<ItemPickup>();
        objToSpawn.GetComponent<ItemPickup>().radius = 100;
        objToSpawn.AddComponent<BoxCollider2D>();
        objToSpawn.AddComponent<SpriteRenderer>();
        objToSpawn.transform.position = newSpawnPosition;
        objToSpawn.transform.localScale = new Vector3(100, 100, 1);
        objToSpawn.GetComponent<SpriteRenderer>().sortingLayerName = "Decorations above player";
    }

}

